package com.sonalake;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import java.io.File;
import java.util.List;
import org.junit.Test;

public class Example {

    @Test
    public void verifyForm() {

        //open the sonalake main page
        open("http://www.sonalake.com");
        //list all menu items
        List<SelenideElement> listOfMenuItems = $$("li");
        //verify if HOME menu item is available
        listOfMenuItems.get(0).shouldHave(Condition.exactText("HOME"));
        //verify if ABOUT menu item is available
        listOfMenuItems.get(1).shouldHave(Condition.exactText("ABOUT"));
        //verify if SERVICES menu item is available
        listOfMenuItems.get(2).shouldHave(Condition.exactText("SERVICES"));
        //verify if OUR WORK menu item is available
        listOfMenuItems.get(3).shouldHave(Condition.exactText("OUR WORK"));
        //verify if CAREERS menu item is available
        listOfMenuItems.get(4).shouldHave(Condition.exactText("CAREERS"));
        //verify if NEWS & BLOG menu item is available
        listOfMenuItems.get(5).shouldHave(Condition.exactText("NEWS & BLOG"));

        //look for CAREERS menu item and click it
        $$("li").stream().filter(el -> el.getText().contains("CAREERS"))
                .findFirst().get().click();

        //Fill the form
        //first name
        $$("input").stream().filter(el -> el.getAttribute("name").contains("your-name"))
                .findFirst().get().setValue("John");
        //email
        $$("input").stream().filter(el -> el.getAttribute("name").contains("your-email"))
                .findFirst().get().setValue("john@sonalake.com");
        //email topic
        $$("input").stream().filter(el -> el.getAttribute("name").contains("your-topic"))
                .findFirst().get().setValue("I want to work here");
        //attach file
        $(".wpcf7-file").uploadFile(new File("src/CV_techtalk.pdf"));
        //type some text
        $("textarea").setValue("some text");
        //resolve captcha
        $("input[name='capital-quiz']").setValue("8");
        //accept all agreements
        $$("input.pi-acceptance").stream().forEach(el -> { el.click();});
        //click submit
        $(".askjobbutton").click();
    }
}
