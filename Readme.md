This example is related to the post available on official Sonalake page:
https://sonalake.com/latest/selenide-a-powerful-testing-framework/

## How to run this code
Open src/test/java/com/sonalake/Example.java
Enable auto-import / or just import the changes to create project structure
Run the code.

## Project description
Each line of code has a short description of its purpose